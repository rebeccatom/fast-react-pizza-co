import React, { useState } from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Route, Routes, useNavigate } from "react-router-dom";

import "./index.css";

const pizzaData = [
  {
    name: "Focaccia",
    ingredients: "Bread with italian olive oil and rosemary",
    price: 6,
    photoName: "pizzas/focaccia.jpg",
    soldOut: false,
  },
  {
    name: "Pizza Margherita",
    ingredients: "Tomato and mozarella",
    price: 10,
    photoName: "pizzas/margherita.jpg",
    soldOut: false,
  },
  {
    name: "Pizza Spinaci",
    ingredients: "Tomato, mozarella, spinach, and ricotta cheese",
    price: 12,
    photoName: "pizzas/spinaci.jpg",
    soldOut: false,
  },
  {
    name: "Pizza Funghi",
    ingredients: "Tomato, mozarella, mushrooms, and onion",
    price: 12,
    photoName: "pizzas/funghi.jpg",
    soldOut: false,
  },
  {
    name: "Pizza Salamino",
    ingredients: "Tomato, mozarella, and pepperoni",
    price: 15,
    photoName: "pizzas/salamino.jpg",
    soldOut: true,
  },
  {
    name: "Pizza Prosciutto",
    ingredients: "Tomato, mozarella, ham, aragula, and burrata cheese",
    price: 18,
    photoName: "pizzas/prosciutto.jpg",
    soldOut: false,
  },
];

function App() {
  const [pizzas, setPizzas] = useState(pizzaData);

  function togglePizzaSoldOut(pizzaName) {
    setPizzas(
      pizzas.map((pizza) =>
        pizza.name === pizzaName ? { ...pizza, soldOut: !pizza.soldOut } : pizza
      )
    );
  }

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home pizzas={pizzas} />} />
        <Route
          path="/employees"
          element={
            <Employees
              pizzas={pizzas}
              togglePizzaSoldOut={togglePizzaSoldOut}
            />
          }
        />
      </Routes>
    </BrowserRouter>
  );

  function Home({ pizzas }) {
    return (
      <div className="container">
        <Header />
        <Menu pizzas={pizzas} />
        <Footer />
      </div>
    );
  }
}

function Pizza({ pizzaObj }) {
  // if (pizzaObj.soldOut) return null;
  return (
    <li className={`pizza ${pizzaObj.soldOut ? "sold-out" : ""}`}>
      <img src={pizzaObj.photoName} alt={pizzaObj.name} />
      <div>
        <h3>{pizzaObj.name}</h3>
        <p>{pizzaObj.ingredients}</p>
        <span>{pizzaObj.soldOut ? "Sold out" : pizzaObj.price}</span>
      </div>
    </li>
  );
}

function Header() {
  return (
    <header className="header">
      <h1>Fast React Pizza Co.</h1>
    </header>
  );
}

function Menu({ pizzas }) {
  const navigate = useNavigate();
  const numPizzas = pizzas.length;

  return (
    <>
      <button className="employee-btn" onClick={() => navigate("/employees")}>
        Employees Only! 🙅🏻‍♀️
      </button>
      <main className="menu">
        <h2>Our Menu</h2>
        {numPizzas > 0 ? (
          <>
            <p>
              Authentic Italian cuisine. 6 creative dishes to choose from. All
              from our stone, oven, all organic, all delicious.
            </p>
            <ul className="pizzas">
              {pizzas.map((pizza) => (
                <Pizza pizzaObj={pizza} key={pizza.name} />
              ))}
            </ul>
          </>
        ) : (
          <p>We're still working on our menu. Please come back later :)</p>
        )}
      </main>
    </>
  );
}

function Footer() {
  const hour = new Date().getHours();
  const openHour = 12;
  const closeHour = 22;
  const isOpen = hour >= openHour && hour <= closeHour;

  return (
    <footer className="footer">
      {isOpen ? (
        <Order openHour={openHour} closeHour={closeHour} />
      ) : (
        <p>
          We're happy to welcome you between {openHour}:00 and {closeHour}:00.
        </p>
      )}
    </footer>
  );
}

function Order({ closeHour, openHour }) {
  return (
    <div className="order">
      <p>
        We're open from {openHour} until {closeHour}:00. Come visit us or order
        online.
      </p>
      <button className="btn">Order</button>
    </div>
  );
}

// ************************

function Employees({ pizzas, togglePizzaSoldOut }) {
  const [soldOutIsOpen, setSoldOutIsOpen] = useState(false);
  const navigate = useNavigate();

  function handleToggleSoldOut(pizzaName) {
    togglePizzaSoldOut(pizzaName);
  }

  return (
    <>
      <button className="employee-btn" onClick={() => navigate("/")}>
        Back to Menu 👩🏻‍🍳
      </button>
      <main className="menu">
        <h2>Employee Options</h2>
        <div className="toggle-btn-container">
          <button
            className="btn"
            onClick={() => setSoldOutIsOpen(!soldOutIsOpen)}
          >
            Toggle Sold Out
          </button>
        </div>
        {soldOutIsOpen && (
          <div className="button-grid">
            {pizzas.map((pizza) => (
              <div
                key={pizza.name}
                onClick={() => handleToggleSoldOut(pizza.name)}
              >
                <button className="option-btn">
                  {pizza.name} {pizza.soldOut ? "✅" : "❌"}
                </button>
              </div>
            ))}
          </div>
        )}
        {/* Other employee options... */}
      </main>
    </>
  );
}

// React v18
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
